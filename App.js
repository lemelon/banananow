import React from 'react';
import {Text, View, Button, StyleSheet} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import {RNCamera} from 'react-native-camera';
import BarcodeScanner from './components/barCodeScanner';

import {colors, fonts, padding, dimensions} from './styles/base.js';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.header}>Welcome to banananow</Text>
        <Button
          title="Get started"
          onPress={() => this.props.navigation.navigate('Details')}
        />
        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
          <View
            style={{width: 50, height: 50, backgroundColor: 'powderblue'}}
          />
          <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
          <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
        </View>
      </View>
    );
  }
}

class PhotoScreen extends React.Component {
  render() {
    return (
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={{
          flex: 1,
          width: '100%',
        }}
      />
    );
  }
}

class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>Did you just buy some food?</Text>
        <Button
          title="Take a photo"
          onPress={() => this.props.navigation.navigate('Photo')}
        />
        <Text>Or</Text>
        <Button
          title="Scan item"
          onPress={() => this.props.navigation.navigate('Scan')}
        />
      </View>
    );
  }

  takePicture = async () => {
    if (this.camera) {
      const options = {quality: 0.5, base64: true};
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
    }
  };
}

const AppNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
    Photo: PhotoScreen,
    Scan: BarcodeScanner,
  },
  {
    initialRouteName: 'Home',
  },
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: padding.sm,
    paddingVertical: padding.lg,
    width: dimensions.fullWidth,
  },
  header: {
    fontSize: fonts.lg,
    fontFamily: fonts.primary,
    backgroundColor: 'skyblue',
  },
  section: {
    paddingVertical: padding.lg,
    paddingHorizontal: padding.xl,
  },
});
